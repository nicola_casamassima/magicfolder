﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.CognitiveServices.Language.TextAnalytics;
using Microsoft.Azure.CognitiveServices.Language.TextAnalytics.Models;



namespace CognitiveServices
{
    public class TextAnalysisManager
    {
        private ITextAnalyticsAPI client;

        public TextAnalysisManager()
        {
            // Create a client.
            client = new TextAnalyticsAPI();
            client.AzureRegion = AzureRegions.Westeurope;
            client.SubscriptionKey = ConfigurationManager.AppSettings["TextAPIPrimaryKey"];
        }

        public List<string> GetKeyPhrasesFromText(string text)
        {
            var keysResult = new List<string>();
            
            // Getting language
            LanguageBatchResult result = client.DetectLanguage(
                    new BatchInput(
                        new List<Input>()
                        {
                          new Input("1", text),
                        }));

            var language = "en";

            // Printing language results.
            foreach (var document in result.Documents)
            {
                language = document.DetectedLanguages[0].Iso6391Name;
            }

            // Getting key-phrases
            KeyPhraseBatchResult result2 = client.KeyPhrases(
                    new MultiLanguageBatchInput(
                        new List<MultiLanguageInput>()
                        {
                          new MultiLanguageInput(language, "1", text),
                        }));
            
            // Printing keyphrases
            foreach (var document in result2.Documents)
            {
                foreach (string keyphrase in document.KeyPhrases)
                {
                    keysResult.Add(keyphrase);
                }
            }

            return keysResult;
        }
    }
}
