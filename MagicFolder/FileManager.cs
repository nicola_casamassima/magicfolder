﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using Microsoft.Office.Interop.Word;

namespace H_01.FileCrawler
{
    public class FileManager
    {
        public string ExtractTextFromFile(string fileName)
        {
            var fInfo = new System.IO.FileInfo(fileName);

            switch (fInfo.Extension)
            {
                case ".pdf":
                    return GetTextFromPDF(fileName);
                case ".txt":
                    return System.IO.File.ReadAllText(fileName);
                case ".docx":
                case ".doc":
                    return GetTextFromWord(fileName);
                default:
                    return string.Empty;
            }
        }

        public string GetFileInfo(string fileName)
        {
            var fInfo = new System.IO.FileInfo(fileName);
            var fAccess = System.IO.File.GetAccessControl(fileName);

            return fInfo.DirectoryName + "," +
                fInfo.Name + "," +
                fAccess.GetOwner(typeof(System.Security.Principal.NTAccount)).ToString() + "," +
                fInfo.LastWriteTime;
        }

        public void SaveCSV(string fileName, List<string> data)
        {
            System.IO.File.WriteAllLines(fileName, data);
        }

        public Dictionary<string, string> GetKeyPathsFromCSV(string fileName)
        {
            var result = new Dictionary<string, string>();

            using (var reader = new StreamReader(fileName))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    if (!result.ContainsKey(values[0]))
                        result.Add(values[0], values[1]);
                }
            }

            return result;
        }

        public Dictionary<string, List<string>> GetAllFilesFromSubfolders(string folderPath, string folderId)
        {
            Dictionary<string, List<string>> result = new Dictionary<string, List<string>>();

            var dirInfo = new System.IO.DirectoryInfo(folderPath);

            if (!result.ContainsKey(folderId))
                result.Add(folderId, new List<string>());

            result[folderId].AddRange(dirInfo.GetFiles().Select(f => f.FullName));

            var directories = dirInfo.GetDirectories();
            int fId = int.Parse(folderId + "1");

            foreach (var dir in directories)
            {
                result = result.Concat(GetAllFilesFromSubfolders(dir.FullName, fId.ToString())).ToDictionary(v => v.Key, v => v.Value);
                fId++;
            }
            
            return result;
        }

        public void ReindexDictionary(Dictionary<string, List<string>> result)
        {
            int maxKeyLenght = result.Keys.Max(k => k.Length);
            Dictionary<string, string> keysToUpdate = new Dictionary<string, string>();

            foreach (var res in result)
            {
                int l = res.Key.Length;

                if (l < maxKeyLenght)
                {
                    var newKey = res.Key;

                    while (l < maxKeyLenght)
                    {
                        newKey += "0";
                        l++;
                    }

                    keysToUpdate.Add(res.Key, newKey);
                }
            }

            foreach (var keyToUpdate in keysToUpdate)
            {
                result.Add(keyToUpdate.Value, result[keyToUpdate.Key]);
                result.Remove(keyToUpdate.Key);
            }
        }

        private string GetTextFromPDF(string fileName)
        {
            StringBuilder text = new StringBuilder();
            using (PdfReader reader = new PdfReader(fileName))
            {
                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    text.Append(PdfTextExtractor.GetTextFromPage(reader, i));
                }
            }

            return text.ToString();
        }

        private string GetTextFromWord(string fileName)
        {
            StringBuilder text = new StringBuilder();
            Application word = new Application();
            object miss = System.Reflection.Missing.Value;
            object path = fileName;
            object readOnly = true;
            Microsoft.Office.Interop.Word.Document docs = word.Documents.Open(ref path, ref miss, ref readOnly, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss);

            for (int i = 0; i < docs.Paragraphs.Count; i++)
            {
                text.Append(" \r\n " + docs.Paragraphs[i + 1].Range.Text.ToString());
            }

            return text.ToString();
        }
    }

}
