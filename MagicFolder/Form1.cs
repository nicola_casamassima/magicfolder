﻿using CognitiveServices;
using MLServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace H_01.FileCrawler
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private string folder;

        private void OpenFileButton_Click(object sender, EventArgs e)
        {
            List<string> fileKeys = new List<string>();
            var fileManager = new FileManager();

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                foreach (var fileName in openFileDialog1.FileNames)
                {
                    var fileText = fileManager.ExtractTextFromFile(fileName);

                    var text = CognitiveCheck.Checked ? string.Join(" ", new TextAnalysisManager().GetKeyPhrasesFromText(fileText)) : 
                        fileText;

                    fileKeys.Add(fileManager.GetFileInfo(fileName) + "," + text);
                }
            }

            fileManager.SaveCSV("data.csv", fileKeys);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                folder = folderBrowserDialog1.SelectedPath;
            }
        }

        private void OpenFolderButton_Click(object sender, EventArgs e)
        {
            List<string> fileKeys = new List<string>();
            var fileManager = new FileManager();

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                fileKeys.Add("IDFolder,Path,FileName,CreatedBy,Modified,Text");
                DirectoryInfo dirInfo = new DirectoryInfo(folderBrowserDialog1.SelectedPath);

                foreach (var fileName in dirInfo.GetFiles())
                {
                    var fileText = fileManager.ExtractTextFromFile(fileName.FullName).Replace("\n", " ").Replace("\t", " ").Replace("\r", " ")
                        .Replace(",", "").Replace("  ", " ").Replace("  ", " ");

                    var text = CognitiveCheck.Checked ? string.Join(" ", new TextAnalysisManager().GetKeyPhrasesFromText(fileText)) :
                        fileText;

                    fileKeys.Add("0," + fileManager.GetFileInfo(fileName.FullName) + "," + text);
                }

                int fId = 1;
                var fileInFolders = new Dictionary<string, List<string>>();

                foreach (var dirName in dirInfo.GetDirectories())
                {
                    fileInFolders = fileInFolders.Concat(fileManager.GetAllFilesFromSubfolders(dirName.FullName, fId.ToString()))
                        .ToDictionary(k => k.Key, k => k.Value);

                    fId++;
                }

                fileManager.ReindexDictionary(fileInFolders);

                foreach (var files in fileInFolders)
                {
                    foreach (var file in files.Value)
                    {
                        var fileText = fileManager.ExtractTextFromFile(file).Replace("\n", " ").Replace("\t", " ").Replace("\r", " ")
                        .Replace(",", "").Replace("  ", " ").Replace("  ", " ");

                        var text = CognitiveCheck.Checked ? string.Join(" ", new TextAnalysisManager().GetKeyPhrasesFromText(fileText)) :
                            fileText;

                        fileKeys.Add(files.Key + "," + fileManager.GetFileInfo(file) + "," + text);
                    }
                }

            }

            fileManager.SaveCSV("data.csv", fileKeys);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                foreach (var fileName in openFileDialog1.FileNames)
                {
                    new Controller().PlaceFileWithML(fileName);
                }
            }
        }
    }
}
