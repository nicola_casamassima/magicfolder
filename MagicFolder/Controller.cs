﻿using CognitiveServices;
using MLServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H_01.FileCrawler
{
    public class Controller
    {
        private string dataFilePath = ConfigurationManager.AppSettings["DataFilePath"];

        public void PlaceFileWithML(string fileName, bool useCognitiveServices = false)
        {
            var fileManager = new FileManager();
            string result = string.Empty;

            var fileText = fileManager.ExtractTextFromFile(fileName);

            var text = useCognitiveServices ? string.Join(" ", new TextAnalysisManager().GetKeyPhrasesFromText(fileText)) :
                fileText;

            List<Task> t = new List<Task>{Task.Factory.StartNew(() =>
                    {
                        result = PredictFolderService.InvokeRequestResponseService(fileName, text).Result;
                    }) };

            Task.WaitAll(t.ToArray());

            var keyPaths = fileManager.GetKeyPathsFromCSV(dataFilePath);

            dynamic jsonObj = JsonConvert.DeserializeObject(result);

            double maxAccuracy = 0;
            string maxAccuracyPathKey = string.Empty;
            string maxAccuracyPath = string.Empty;

            foreach (Newtonsoft.Json.Linq.JProperty prob in jsonObj.Results.output1[0])
            {
                double d = 0;

                if (prob.Name.Contains("Scored Probabilities"))
                    double.TryParse(prob.Value.ToString(), out d);

                if (d > maxAccuracy)
                {
                    maxAccuracy = d;
                    maxAccuracyPathKey = prob.Name.Split('\"')[1];
                }
            }

            if (maxAccuracy >= double.Parse(ConfigurationManager.AppSettings["MinAccuracy"]))
            {
                maxAccuracyPathKey = jsonObj.Results.output1[0]["Scored Labels"];
                maxAccuracyPath = keyPaths[maxAccuracyPathKey];
            }
            else
            {
                maxAccuracyPath = keyPaths[maxAccuracyPathKey];
                maxAccuracyPath = new DirectoryInfo(maxAccuracyPath).Parent.FullName;
                maxAccuracyPath = Directory.CreateDirectory(Path.Combine(maxAccuracyPath, "New Folder")).FullName;
            }

            File.Copy(fileName, Path.Combine(maxAccuracyPath, fileName.Split('\\').Last()));

            Process.Start(maxAccuracyPath);
        }
    }
}
