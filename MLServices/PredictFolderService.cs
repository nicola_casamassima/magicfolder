﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http.Formatting;

namespace MLServices
{
    public class PredictFolderService
    {
        public static async Task<string> InvokeRequestResponseService(string fileName, string text)
        {
            using (var client = new HttpClient())
            {
                var scoreRequest = new
                {
                    Inputs = new Dictionary<string, List<Dictionary<string, string>>>() {
                        {
                            "input1",
                            new List<Dictionary<string, string>>(){new Dictionary<string, string>(){
                                            {
                                                "IDFolder", string.Empty
                                            },
                                            {
                                                "Path", string.Empty
                                            },
                                            {
                                                "FileName", fileName
                                            },
                                            {
                                                "CreatedBy", string.Empty
                                            },
                                            {
                                                "Modified", string.Empty
                                            },
                                            {
                                                "Text", text
                                            },
                                            {
                                                "Column 6", string.Empty
                                            },
                                }
                            }
                        },
                    },
                    GlobalParameters = new Dictionary<string, string>()
                    {
                    }
                };

                const string apiKey = "5wyVTejIhaxPm2vRr+MElZ+7vqxNTXrrrUCBsxVuwdoVVyjT+Y77XZ3c9v+Nfu4t0BX1EiLzyzOLaw6bfDmAfA=="; // Replace this with the API key for the web service
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", apiKey);
                client.BaseAddress = new Uri("https://ussouthcentral.services.azureml.net/workspaces/7eba798133ad418592124f088eeee833/services/d2fe8f8bde544ee08968b68661c7eccc/execute?api-version=2.0&format=swagger");
                
                HttpResponseMessage response = await client.PostAsJsonAsync("", scoreRequest);

                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();
                    return result;
                }
                else
                {
                    Console.WriteLine(string.Format("The request failed with status code: {0}", response.StatusCode));

                    // Print the headers - they include the requert ID and the timestamp,
                    // which are useful for debugging the failure
                    Console.WriteLine(response.Headers.ToString());

                    string responseContent = await response.Content.ReadAsStringAsync();
                    Console.WriteLine(responseContent);
                }
            }

            return string.Empty;
        }
    }
}
